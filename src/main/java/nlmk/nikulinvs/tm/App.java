package nlmk.nikulinvs.tm;


import nlmk.nikulinvs.tm.dao.ProjectDAO;
import nlmk.nikulinvs.tm.dao.TaskDAO;
import nlmk.nikulinvs.tm.entity.Project;
import nlmk.nikulinvs.tm.entity.Task;

import java.util.Scanner;

import static nlmk.nikulinvs.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */

public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    static {
        projectDAO.create("PROJECT DEMO 1");
        projectDAO.create("PROJECT DEMO 2");
        taskDAO.create("TASK DEMO 1");
        taskDAO.create("TASK DEMO 2");
    }

    public static void main(final String[] args) {
        run(args);
        displayWelcom();
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
        }
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }


    private static int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case CMD_VERSION: return displayVersion();
            case CMD_ABOUT: return displayAbout();
            case CMD_HELP: return displayHelp();
            case CMD_EXIT: return displayExit();

            case CMD_PROJECT_CREATE: return createProject();
            case CMD_PROJECT_CLEAR: return clearProject();
            case CMD_PROJECT_LIST: return listProject();
            case CMD_PROJECT_VIEW: return viewProjectByIndex();
            case CMD_PROJECT_REMOVE_BY_ID: return removeProjectById();
            case CMD_PROJECT_REMOVE_BY_NAME: return removeProjectByName();
            case CMD_PROJECT_REMOVE_BY_INDEX: return removeProjectByIndex();
            case CMD_PROJECT_UPDATE_BY_INDEX: return updateProjectByIndex();

            case CMD_TASK_CREATE: return createTask();
            case CMD_TASK_CLEAR: return clearTaskt();
            case CMD_TASK_LIST: return listTask();
            case CMD_TASK_VIEW: return viewTaskByIndex();
            case CMD_TASK_REMOVE_BY_ID: return removeTaskById();
            case CMD_TASK_REMOVE_BY_NAME: return removeTaskByName();
            case CMD_TASK_REMOVE_BY_INDEX: return removeTasktByIndex();
            case CMD_TASK_UPDATE_BY_INDEX: return updateTaskByIndex();

            default: return displayError();
        }
    }

    //Start Block Project
    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description");
        final String description = scanner.nextLine();
        projectDAO.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("Please, enter index number");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = projectDAO.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description");
        final String description = scanner.nextLine();
        projectDAO.update(project.getId(),name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        final Project project = projectDAO.removeByName(name);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY NAME:" + name + " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("Please, enter ID number");
        final Long id = scanner.nextLong();
        final Project project = projectDAO.removeById(id);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY ID:" + id + " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("Please, enter INDEX number");
        final int id = scanner.nextInt() -1;
        final Project project = projectDAO.removeByIndex(id);
        if (project == null)
            System.out.println("[FAILE -> PROJECT BY INDEX:" + id + " IS NOT EXIST]" );
        else System.out.println("[OK]");
        return 0;
    }

    private static int clearProject() {
        System.out.println("[CLEARE PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static  void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID:-> " + project.getId());
        System.out.println("NAME:->" + project.getName());
        System.out.println("DESCRIPTION:->" + project.getDescription());
        System.out.println("[OK]");
    }

    private static  void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID:->" + task.getId());
        System.out.println("NAME:->" + task.getName());
        System.out.println("DESCRIPTION:->" + task.getDescription());
        System.out.println("[OK]");
    }

    public static int viewTaskByIndex () {
        System.out.println("ENTER TASK INDEX");
        final int index  = scanner.nextInt()-1;
        final Task task = taskDAO.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public static int viewProjectByIndex () {
        System.out.println("ENTER PROJECT INDEX");
        final int index  = scanner.nextInt() -1;
        final Project project = projectDAO.findByIndex(index);
        viewProject(project);
        return 0;
    }

    private static int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project: projectDAO.findAll()) {
            System.out.println(index + ") " + project.getId() + " : " + project.getName());
            index ++;
        }
        System.out.println("[OK]");
        return 0;
    }
    //End Block Project

    //Start Block Task
    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Please, enter task name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskDAO.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Please, enter index number");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Task task = taskDAO.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Please, enter task name");
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description");
        final String description = scanner.nextLine();
        taskDAO.update(task.getId(),name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int removeTaskByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("Please, enter project name");
        final String name = scanner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if (task == null)
            System.out.println("[FAILE -> TASK BY NAME:" + name + " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int removeTaskById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("Please, enter ID number");
        final Long id = scanner.nextLong();
        final Task task = taskDAO.removeById(id);
        if (task == null)
            System.out.println("[FAILE -> TASK BY ID:" + (id+1)+ " IS NOT EXIST]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int removeTasktByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("Please, enter INDEX number");
        final int id = scanner.nextInt() -1;
        final Task task = taskDAO.removeByIndex(id);
        if (task == null)
            System.out.println("[FAILE -> TASK BY INDEX:" + (id+1) + " IS NOT EXIST]" );
        else System.out.println("[OK]");
        return 0;
    }

    private static int clearTaskt() {
        System.out.println("[CREATE TASK]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listTask() {
        System.out.println("[CREATE TASK]");
        int index = 1;
        for (final Task task: taskDAO.findAll()) {
            System.out.println(index + ") " + task.getId() + " : " + task.getName());
            index ++;
        }
        System.out.println("[OK]");
        return 0;
    }
    //End Block Task

    private static int displayExit() {
        System.out.println("Terminate programm...");
        return 0;
    }

    private static int displayError() {

        System.out.println("Error! Unknown programm arguments ...");
        return -1;
    }

    private static void displayWelcom() {

        System.out.println("**Welcom to Task Manager**");
        }

    private static int displayHelp() {
        System.out.println("|-------HELP LIST--------------------------|");
        System.out.println("version - Display programm version");
        System.out.println("about - Display developer info");
        System.out.println("help - Display list of terminal commands");
        System.out.println("exit - Terminate Task-manager!");
        System.out.println("--------PROJECT------------------------------");
        System.out.println("project-create - Create new project by name");
        System.out.println("project-clear - Remove all projects");
        System.out.println("project-list - Display list of projects");
        System.out.println("pproject-remove-by-name - Remov exist projects by name");
        System.out.println("pproject-remove-by-id - Remov exist projects by Id");
        System.out.println("pproject-remove-by-index - Remov exist projects by index");
        System.out.println("pproject-update-by-index - Update exist projects by index");
        System.out.println("--------TASK----------------------------------");
        System.out.println("task-create - Create new task by name");
        System.out.println("task-clear - Remove all tasks");
        System.out.println("task-list - Display list of tasks");
        System.out.println("task-remove-by-name - Remov exist task by name");
        System.out.println("task-remove-by-id - Remov exist task by Id");
        System.out.println("task-remove-by-index - Remov exist task by index");
        System.out.println("task-update-by-index - Update exist taskhelp by index");
        return 0;
        }

    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
        }

    private static int displayAbout() {
        System.out.println("Nikulin Viktor");
        System.out.println("nikulin_vs@nlmk.com");
        return 0;
        }

}
